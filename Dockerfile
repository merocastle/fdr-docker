FROM centos:6
LABEL maintainer="hlee@bills.com"
EXPOSE 80

# Install requirements
RUN rm /bin/sh && ln -s /bin/bash /bin/sh
RUN yum -y update && yum -y upgrade
RUN yum install -y yum-utils centos-release-scl epel-release 

# Install vim
RUN yum install -y vim
RUN touch ~/.vimrc
RUN echo "set nu" >> ~/.vimrc \
  && echo "set ts=2 sw=2 et" >> ~/.vimrc \
  && echo "set ai" >> ~/.vimrc \ 
  && echo "set paste" >> ~/.vimrc

# Install curl
RUN yum install -y curl

# Install wget
RUN yum install -y wget

# Install git
RUN yum install -y git


# Install node through nvm
# nvm environment variables
ENV NVM_DIR /usr/local/.nvm
ENV NODE_VERSION 7.4.0 
RUN curl --silent -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.6/install.sh | bash
RUN . $NVM_DIR/nvm.sh \
  && nvm --version \
  && nvm install $NODE_VERSION
ENV NODE_PATH $NVM_DIR/versions/node/$NODE_VERSION/lib/node_modules
ENV PATH "$NVM_DIR/versions/node/$NODE_VERSION/bin:$PATH"

# Install PHP
RUN yum install -y rh-php56 rh-php56-php
RUN . /opt/rh/rh-php56/enable
RUN yum install -y sclo-php56-php-mcrypt rh-php56-php-pdo rh-php56-php-mysqlnd rh-php56-php-mongo \
	rh-php56-php-apcu rh-php56-php-soap rh-php56-php-pecl-memcache rh-php56-php-mbstring rh-php56-php-bcmath\
	rh-php56-php-intl	
COPY config/php.ini /etc/opt/rh/rh-php56/
ENV PATH "$PATH:/opt/rh/rh-php56/root/usr/bin"
# php.ini location => /etc/opt/rh/rh-php56/php.ini

# Install Composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Install Ruby
RUN gpg --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3 7D2BAF1CF37B13E2069D6956105BD0E739499BDB
RUN \curl -L https://get.rvm.io | bash -s stable
RUN /bin/bash -l -c "rvm requirements"
RUN /bin/bash -l -c "rvm install 2.4.2"
RUN /bin/bash -l -c "rvm use 2.4.2 --default"
#RUN /bin/bash -l -c "gem sources -r https://rubygems.org -a http://rubygems.org"
RUN /bin/bash -l -c "gem install bundler --no-ri --no-rdoc"
RUN /bin/bash -l -c "gem install sass --no-ri --no-rdoc"
RUN /bin/bash -l -c "gem install compass --no-ri --no-rdoc"

# Install httpd2.4
ENV HTTPD_DIR /opt/rh/httpd24/root/etc/httpd
RUN yum install -y httpd24
RUN scl enable httpd24 bash

# Configure httpd
COPY config/httpd/httpd.conf $HTTPD_DIR/conf
RUN mkdir -pv $HTTPD_DIR/conf/extra/
COPY config/httpd/extra/httpd-vhosts.conf $HTTPD_DIR/conf/extra/

# Configure app
RUN mkdir -pv /usr/src/billsdev/
ENV APP_ROOT /usr/src/billsdev/
ENV APP_ENV "local"
RUN /bin/bash -l -c "echo export APPLICATION_ENV=$APP_ENV >> ~/.bashrc"
VOLUME $APP_ROOT
ENV PATH "$PATH:/usr/sbin"

CMD ["scl", "enable", "httpd24", "bash"]
