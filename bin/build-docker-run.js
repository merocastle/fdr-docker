const fs = require('fs-extra');
const path = require('path');

function createDockerRun() {
  let targetDir = path.join(__dirname, '../../');

  let command = `docker run --name billsdev-app --rm -it -p 80:80 -v ${targetDir}:/usr/src/billsdev billsdev`;

  let _filepath = path.join(__dirname, 'docker-run');

  fs.outputFile(_filepath, command, (err) => {
    if (err) return console.error(err);
    fs.chmodSync(_filepath, 0755);
    console.log(`An executable file to run a docker container is created at ${_filepath}`);
  });
}

module.exports = createDockerRun;
