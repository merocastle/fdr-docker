const https = require('https');
const fs = require('fs-extra');
const tar = require('tar');
const path = require('path');

const createDockerRun = require('./build-docker-run');

// Create a script to run a docker container.
createDockerRun();

// Create a /lib directory to save external libraries and dependencies.
let libDir = path.join(__dirname, '../lib/');
if (!fs.existsSync(libDir)) fs.mkdirSync(libDir);

// Create a /logs directory and log files.
let logDir = path.join(__dirname, '../logs/');
if (!fs.existsSync(logDir)) {
	fs.mkdirSync(logDir);
	fs.ensureFileSync(path.join(logDir, '/access_log'));
	fs.ensureFileSync(path.join(logDir, '/error_log'));
}


// The information of ZendFramework.
let zendFramework = {
	url: `https://packages.zendframework.com/releases/ZendFramework-1.11.11/ZendFramework-1.11.11.tar.gz`,
	name: `ZendFramework-1.11.11.tar.gz`,
	dest: libDir,
}

// Download ZendFramework1.11.11
downloadFile(zendFramework.url, zendFramework.name, zendFramework.dest, function onDownloaded(err, file) {
  let src = file.path;
  let dest = libDir;
	extractTar(src, dest);
});

/*
* Download a file over the HTTPS.
* requires 'https' and 'fs' modules.
* @param url {string} the URL to download the source file
* @param name {string} the name of the file to be saved.
* @param dest {string} the relative path where the downloaded to be saved.
* @param callback {function} a callback funtion to be invoked when the task completed.
*/
function downloadFile(url, name, dest, callback) {
	console.info(`Start downloading ${name}`);
  let file = fs.createWriteStream(`${dest}${name}`);
  https.get(url, response => {
    response.pipe(file);
    file.on('finish', () => {
      console.info(`Completed downloading ${name}`);	
      callback(null, file);
    });
  });
}

/*
* Extract tar.gz files.
* requires 'tar' and 'fs' modules.
* @param src {string} the path to the tar.gz file
* @param src {string} the path where the tar.gz file to be extracted
*/
function extractTar(src, dest) {
  console.info(`Start unzipping ${src} into ${dest}.`);
  let stream = fs.createReadStream(src);
  stream
    .pipe(tar.extract({
      cwd: dest, 
    }));
	console.info(`Completed upzipping ${src} into ${dest}.`);
}
