# FDR Docker

FDR Docker provides a container where FDR applications can run without local setup issues.

## How to organize the applications

FDR Docker will automatically look up the parent directory, and all applications on that parent directory
will be under the the FDR docker's environment. Please see the example app organizations below.

    .
    ├──	common 
    ├── fdr-client
    ├──	fdr-cap 
    ├── fdr-platform
    ├──	fdr-legacy 
    ├──	freedom
    └──	fdr-docker 

## How to build your docker container

Type the following commands below. Once you are done, your docker image will be ready to be run on your local machine.

```
[Local Machine Terminal]
cd path-to/fdr-docker

npm install
npm run build
npm run docker-build
```

## How to run the docker container

The command below will run the docker container and give you the full set of the local setup needed to run FDR applications.

```
[Local Machine Terminal]
npm run docker-run
```

It will bring you to the terminal in the docker container. To run the server, type the command below.

```
[Docker Container Terminal]
apachectl restart
```

You can find your FDR applications in /usr/src/billsdev/. The docker container provides on the environment where the applications
can be executed properly. You need to build or compile assets for the application manually. Please see the example below.

```
[Docker Container Terminal]
// For example, fdr-client project.
cd /usr/src/billsdev/fdr-client
composer install
npm install

// To build your assets continuously when development status.
[Docker Container Terminal]
npm run dev
```

From the above, we've completed all local setup for FDR Client. Go visit [http://fdr-save.local.billsdev.com/](http://fdr-save.local.billsdev.com/)
For other applications, the urls are http://[app-directory-name].local.billsde.com

## Issues
1. npm run dev throws an error on the freedom application - node-sass fails to locate sass stylesheets when using relative paths in the docker container.
2. Create a local user and run the container under the user - to avoid security issues when running Composer, Node.js, and etc.


## Authors

* **Hoseong Lee** - *Initial work* - hlee@bills.com
